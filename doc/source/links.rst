.. _requirejs: https://requirejs.org/
.. _Django: https://www.djangoproject.com/
.. _webassets: https://webassets.readthedocs.io/en/latest/
.. _django-assets: https://django-assets.readthedocs.io/en/latest/
.. _nodejs: https://nodejs.org/en/