# django_auto_url
django_auto_url is a library for [Django](https://www.djangoproject.com/)
that:

1. automatically generates URLs / URLPatterns for views
2. provides functions and a templatetag to resolve views to URLs.

For detailed information, head to the [documentation](http://django-auto-url.readthedocs.io/). 